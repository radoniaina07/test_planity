export const saveToLocalStorage = <T = any>(key: string, data: T) => {
  if (typeof data === 'string') {
    localStorage.setItem(key, data);
  } else {
    localStorage.setItem(key, JSON.stringify(data));
  }
};

export const getFromLocalStorage = <T = any>(key: string) => {
  const rawData = localStorage.getItem(key);

  if (!rawData) {
    return null;
  }

  try {
    return JSON.parse(rawData) as T;
  } catch (error) {
    return rawData;
  }
};
